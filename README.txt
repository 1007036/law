# Australian Aviation Legislation

#### Air Law Documents

* Civil Aviation Safety Regulations 1998 (CASR1998)
* Civil Aviation Safety Regulations 1998 (CASR1998) Part 61 Manual of Standards
* Civil Aviation Regulations 1988 (CAR1988)
* Civil Aviation Order (CAO)
* Civil Aviation Advisory Publication
* Aeronautical Information Package (AIP) Book
* Aeronautical Information Package (AIP) En Route Supplement Australia (ERSA)

#!/bin/bash

set -e
set -x

if ! type pdftk >/dev/null ; then
  >&2 echo "Missing: pdftk" >&2
  exit 1
fi

if ! type pandoc >/dev/null ; then
  >&2 echo "Missing: pandoc" >&2
  exit 1
fi

if [ -z "$3" ]
then
  >&2 echo "arguments: <download-dir> <index-file> <extract-dir>"
  exit 33
else
  download_dir=${1}
  index_file=${2}
  extract_dir=${3}
fi

extract_car_1988_complete() {
  car1988_complete_dir=${extract_dir}/car1988
  car1988_complete_file=${car1988_complete_dir}/car1988-complete.pdf
  mkdir -p ${car1988_complete_dir}

  pdftk \
    ${download_dir}/car1988/car1988-volume1.pdf \
    ${download_dir}/car1988/car1988-volume2.pdf \
    cat \
    output \
    ${car1988_complete_file}

    echo "Civil Aviation Regulations 1988,Complete,${car1988_complete_file}" >> ${index_file}
}

extract_casr_1998_complete() {
  casr1998_complete_dir=${extract_dir}/casr1998
  casr1998_complete_file=${casr1998_complete_dir}/casr1998-complete.pdf
  mkdir -p ${casr1998_complete_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume1.pdf \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    ${download_dir}/casr1998/casr1998-volume5.pdf \
    cat \
    output \
    ${casr1998_complete_file}

    echo "Civil Aviation Safety Regulations 1998,Complete,${casr1998_complete_file}" >> ${index_file}
}

extract_casr_1998_part61_mos_complete() {
  part61mos_complete_dir=${extract_dir}/casr1998/part61-manual-of-standards
  part61mos_complete_file=${part61mos_complete_dir}/casr1998-part61-manual-of-standards-complete.pdf
  mkdir -p ${part61mos_complete_dir}

  pdftk \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume1.pdf \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume2.pdf \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume3.pdf \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume4.pdf \
    cat \
    output \
    ${part61mos_complete_file}

    echo "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards,Complete,${part61mos_complete_file}" >> ${index_file}
}

extract_casr_1998_part61_mos() {
  if [ $# -lt 5 ]; then
    echo "arguments(5) <volume> <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    part61mos_parts_dir=${extract_dir}/casr1998/part61-manual-of-standards
    mkdir -p ${part61mos_parts_dir}

    pdftk ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume${1}.pdf \
    cat \
    ${4}-${5} \
    output \
    ${part61mos_parts_dir}/${2}.pdf
    echo "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards,${3},${part61mos_parts_dir}/${2}.pdf" >> ${index_file}
  fi
}

extract_casr_1998_part61_mos_parts() {
  extract_casr_1998_part61_mos \
    "1" \
    "casr1998-part61-manual-of-standards-volume1_part61-manual-of-standards" \
    "Volume 1 Part 61 Manual of Standards Instrument 2014" \
    "1" \
    "6"

  extract_casr_1998_part61_mos \
    "1" \
    "casr1998-part61-manual-of-standards-volume1_schedule-1a" \
    "Volume 1 Schedule 1A" \
    "7" \
    "13"

  extract_casr_1998_part61_mos \
    "1" \
    "casr1998-part61-manual-of-standards-volume1_directory-of-units-of-competency-and-units-of-knowledge" \
    "Volume 1 Directory of units of competency and units of knowledge" \
    "14" \
    "end"

  extract_casr_1998_part61_mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_english-language-proficiency" \
    "Volume 2 Section 1 English Language Proficiency" \
    "4" \
    "6"

  extract_casr_1998_part61_mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_common-standards" \
    "Volume 2 Section 2 Common Standards" \
    "7" \
    "20"

  extract_casr_1998_part61_mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_navigation-and-instrument-flying-standards" \
    "Volume 2 Section 3 Navigation and Instrument Flying Standards" \
    "21" \
    "35"

  extract_casr_1998_part61_mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_aircraft-rating-standards" \
    "Volume 2 Section 4 Aircraft Rating Standards" \
    "36" \
    "145"

  extract_casr_1998_part61_mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_operational-rating-and-endorsement-standards" \
    "Volume 2 Section 5 Operational Rating and Endorsement Standards" \
    "146" \
    "236"

  extract_casr_1998_part61_mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_flight-activity-endorsement-standards" \
    "Volume 2 Section 6 Flight Activity Endorsement Standards" \
    "237" \
    "end"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1" \
    "Volume 3 Appendix 1 Flight crew licences and aircraft category ratings" \
    "5" \
    "187"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_basic-aeronautical-knowledge" \
    "Volume 3 Appendix 1 Section 1.1 Basic aeronautical knowledge (BAK)" \
    "5" \
    "19"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_general-aeronautical-knowledge" \
    "Volume 3 Appendix 1 Section 1.2 General aeronautical knowledge (AK)" \
    "20" \
    "36"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_aerodynamics" \
    "Volume 3 Appendix 1 Section 1.3 Aerodynamics (AD)" \
    "37" \
    "42"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_atpl-aircraft-general-knowledge" \
    "Volume 3 Appendix 1 Section 1.4 ATPL Aircraft General Knowledge (AG)" \
    "43" \
    "74"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_flight-rules-and-air-law" \
    "Volume 3 Appendix 1 Section 1.5 Flight Rules and Air Law (FR)" \
    "75" \
    "107"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_human-factors-principles" \
    "Volume 3 Appendix 1 Section 1.6 Human Factors Principles (HF)" \
    "108" \
    "123"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_navigation" \
    "Volume 3 Appendix 1 Section 1.7 Navigation (NV)" \
    "124" \
    "136"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_meteorology" \
    "Volume 3 Appendix 1 Section 1.8 Meteorology (MT)" \
    "137" \
    "154"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_operations-performance-and-planning" \
    "Volume 3 Appendix 1 Section 1.9 Operations Performance and Planning (OP)" \
    "155" \
    "171"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_flight-planning" \
    "Volume 3 Appendix 1 Section 1.10 Flight Planning (FP)" \
    "172" \
    "175"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_atpl-performance-and-loading" \
    "Volume 3 Appendix 1 Section 1.11 ATPL Performance and Loading (PL)" \
    "176" \
    "187"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2" \
    "Volume 3 Appendix 2 Operational Ratings" \
    "188" \
    "216"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_instrument-rating" \
    "Volume 3 Appendix 2 Section 2.1 Instrument Rating" \
    "188" \
    "195"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_private-ifr-rating" \
    "Volume 3 Appendix 2 Section 2.2 Private IFR Rating" \
    "196" \
    "201"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_aerial-application-rating-and-endorsements" \
    "Volume 3 Appendix 2 Section 2.3 Aerial Application Rating and Endorsements" \
    "202" \
    "207"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_instructor-ratings" \
    "Volume 3 Appendix 2 Section 2.4 Instructor Ratings" \
    "208" \
    "210"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_low-level-rating" \
    "Volume 3 Appendix 2 Section 2.5 Low-Level Rating" \
    "211" \
    "213"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_night-vision-imaging-systems-rating" \
    "Volume 3 Appendix 2 Section 2.6 Night Vision Imaging Systems (NVIS) Rating" \
    "214" \
    "214"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_night-vfr-rating" \
    "Volume 3 Appendix 2 Section 2.7 Night VFR Rating" \
    "215" \
    "215"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_examiner-ratings" \
    "Volume 3 Appendix 2 Section 2.8 Examiner Ratings" \
    "216" \
    "216"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-3" \
    "Volume 3 Appendix 3 Aircraft Ratings and Endorsements" \
    "217" \
    "228"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-3_class-ratings" \
    "Volume 3 Appendix 3 Section 3.1 Class Ratings" \
    "217" \
    "217"

  extract_casr_1998_part61_mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-3_type-ratings" \
    "Volume 3 Appendix 3 Section 3.2 Type Ratings" \
    "218" \
    "228"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_section-1-flight-crew-licence-and-associated-category-ratings" \
    "Volume 4 Section 1 Flight Crew Licence and Associated Category Ratings" \
    "4" \
    "9"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_operational-ratings" \
    "Volume 4 Section 2 Operational Ratings" \
    "10" \
    "10"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_foreign-licence-conversion" \
    "Volume 4 Section 3 Foreign Licence Conversion" \
    "11" \
    "12"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_australia-defence-force-conversion" \
    "Volume 4 Section 4 Australian Defence Force (ADF) Conversion" \
    "13" \
    "13"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-g-recreational-pilot-licence" \
    "Volume 4 Schedule 5 Section G Recreational Pilot Licence (RPL)" \
    "16" \
    "20"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-h-private-pilot-licence" \
    "Volume 4 Schedule 5 Section H Private Pilot Licence (PPL)" \
    "21" \
    "26"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-i-commercial-pilot-licence" \
    "Volume 4 Schedule 5 Section I Commercial Pilot Licence (CPL)" \
    "27" \
    "33"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-j-multi-crew-pilot-licence" \
    "Volume 4 Schedule 5 Section J Multi-Crew Pilot Licence (MPL)" \
    "34" \
    "36"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-k-air-transport-pilot-licence" \
    "Volume 4 Schedule 5 Section K Air Transport Pilot Licence (ATPL)" \
    "37" \
    "43"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-l-aircraft-ratings" \
    "Volume 4 Schedule 5 Section L Aircraft Ratings" \
    "44" \
    "63"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-m-instrument-rating" \
    "Volume 4 Schedule 5 Section M Instrument Rating" \
    "64" \
    "66"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-n-private-instrument-rating" \
    "Volume 4 Schedule 5 Section N Private Instrument Rating" \
    "67" \
    "70"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-o-night-vfr-rating" \
    "Volume 4 Schedule 5 Section O Night VFR Rating" \
    "71" \
    "73"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-p-night-vision-imaging-system-rating" \
    "Volume 4 Schedule 5 Section P Night Vision Imaging System (NVIS) Rating" \
    "74" \
    "76"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-q-low-level-rating" \
    "Volume 4 Schedule 5 Section Q Low-Level Rating" \
    "77" \
    "79"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-r-aerial-application-rating" \
    "Volume 4 Schedule 5 Section R Aerial Application Rating" \
    "80" \
    "82"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-t-pilot-instructor-ratings" \
    "Volume 4 Schedule 5 Section T Pilot Instructor Ratings" \
    "83" \
    "88"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-u-flight-examiner-rating" \
    "Volume 4 Schedule 5 Section U Flight Examiner Rating" \
    "89" \
    "91"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-v-flight-engineer-licence" \
    "Volume 4 Schedule 5 Section V Flight Engineer Licence" \
    "92" \
    "92"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-w-flight-engineer-type-rating" \
    "Volume 4 Schedule 5 Section W Flight Engineer Type Rating" \
    "93" \
    "93"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-x-flight-engineer-instructor-rating" \
    "Volume 4 Schedule 5 Section X Flight Engineer Instructor Rating" \
    "94" \
    "94"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-y-flight-engineer-examiner-rating" \
    "Volume 4 Schedule 5 Section Y Flight Engineer Examiner Rating" \
    "95" \
    "95"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-6-proficiency-check-standards" \
    "Volume 4 Schedule 6 Proficiency Check Standards" \
    "96" \
    "110"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-7-flight-review-standards" \
    "Volume 4 Schedule 7 Flight Review Standards" \
    "111" \
    "121"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-8-section-1-flight-tolerances" \
    "Volume 4 Schedule 8 Section 1 Flight Tolerances" \
    "122" \
    "133"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-8-section-2-english-language-proficiency-rating-scales" \
    "Volume 4 Schedule 8 Section 2 English Language Proficiency Rating Scales" \
    "134" \
    "136"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-9-cplh-training-for-non-integrated-training-courses" \
    "Volume 4 Schedule 9 CPL(H) training for paragraph 61.615 (1B) (b) (non-integrated training courses)" \
    "137" \
    "137"

  extract_casr_1998_part61_mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_note-to-part61-manual-of-standards" \
    "Volume 4 Note to Part 61 Manual of Standards (MOS)" \
    "138" \
    "138"
}

extract_casr_1998_part91_mos() {
  if [ $# -lt 4 ]; then
    echo "arguments(4) <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    part91mos_parts_dir=${extract_dir}/casr1998/part91-manual-of-standards
    mkdir -p ${part91mos_parts_dir}

    pdftk ${download_dir}/casr1998/part91-manual-of-standards/casr1998-part91-manual-of-standards.pdf \
    cat \
    ${3}-${4} \
    output \
    ${part91mos_parts_dir}/${1}.pdf
    echo "Civil Aviation Safety Regulations 1998 Part 91 Manual of Standards,${2},${part91mos_parts_dir}/${1}.pdf" >> ${index_file}
  fi
}

extract_casr_1998_part91_mos_parts() {
  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_contents" \
    "Contents" \
    "3" \
    "10"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter1-preliminary" \
    "Chapter 01 Preliminary" \
    "11" \
    "27"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter2-prescriptions-for-certain-definitions-in-the-casr-dictionary" \
    "Chapter 02 Prescriptions for Certain Definitions in the CASR Dictionary" \
    "28" \
    "37"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter3-nvis-flights" \
    "Chapter 03 NVIS Flights" \
    "38" \
    "44"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter4-all-flights-airspeed-limits" \
    "Chapter 04 All Flights - Airspeed Limits" \
    "45" \
    "45"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter5-journey-logs-flights-that-begin-or-end-outside-australian-territory" \
    "Chapter 05 Journey Logs - Flights That Begin or End Outside Australian Territory" \
    "46" \
    "46"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter6-flying-in-formation" \
    "Chapter 06 Flying in Formation" \
    "47" \
    "47"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter7-flight-preparation-weather-assessments-requirements" \
    "Chapter 07 Flight Preparation (Weather Assessments) Requirements" \
    "48" \
    "49"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter8-flight-preparation-alternate-aerodromes-requirements" \
    "Chapter 08 Flight Preparation (Alternate Aerodrome) Requirements" \
    "50" \
    "55"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter9-flight-notifications" \
    "Chapter 09 Flight Notifications" \
    "56" \
    "57"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter10-matters-to-be-checked-before-take-off" \
    "Chapter 10 Matters to be Checked Before Take-off" \
    "58" \
    "60"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter11-air-traffic-services-prescribed-requirements" \
    "Chapter 11 Air Traffic Services - Prescribed Requirements" \
    "61" \
    "73"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter12-minimum-height-rules" \
    "Chapter 12 Minimum Height Rules" \
    "74" \
    "74"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter13-vfr-flights" \
    "Chapter 13 VFR Flights" \
    "75" \
    "75"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter14-ifr-flights" \
    "Chapter 14 IFR Flights" \
    "76" \
    "81"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter15-ifr-take-off-and-landing-minima" \
    "Chapter 15 IFR Take-off and Landing Minima" \
    "82" \
    "87"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter16-approach-ban-for-ifr-flights" \
    "Chapter 16 Approach Ban for IFR Flights" \
    "88" \
    "88"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter17-designated-non-controlled-aerodromes" \
    "Chapter 17 Designated Non-controlled Aerodromes" \
    "89" \
    "89"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter18-safety-when-aeroplane-operating-on-the-ground" \
    "Chapter 18 Safety When Aeroplane Operating on the Ground" \
    "90" \
    "90"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter19-fuel-requirements" \
    "Chapter 19 Fuel Requirements" \
    "91" \
    "95"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter20-safety-of-persons-and-cargo-on-aircraft" \
    "Chapter 20 Safety of Persons and Cargo on Aircraft" \
    "96" \
    "100"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter21-radio-frequency-broadcast-and-reporting-requirements" \
    "Chapter 21 Radio Frequency Broadcast and Reporting Requirements" \
    "101" \
    "106"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter22-performance-based-navigation-pbn" \
    "Chapter 22 Performance-based Navigation (PBN)" \
    "107" \
    "107"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter23-interception-of-aircraft" \
    "Chapter 23 Interception of Aircraft" \
    "108" \
    "108"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter24-take-off-performance" \
    "Chapter 24 Take-off Performance" \
    "109" \
    "110"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter25-landing-performance" \
    "Chapter 25 Landing Performance" \
    "111" \
    "112"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter26-equipment" \
    "Chapter 26 Equipment" \
    "113" \
    "167"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter27-experimental-and-light-sport-aircraft-placards" \
    "Chapter 27 Experimental and Light Sport Aircraft Placards" \
    "168" \
    "168"

  extract_casr_1998_part91_mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter28-requirements-for-minimum-equipment-lists" \
    "Chapter 28 Requirements for Minimum Equipment Lists" \
    "169" \
    "171"
}

extract_casr_1998_part101_mos() {
  if [ $# -lt 4 ]; then
    echo "arguments(4) <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    part101mos_parts_dir=${extract_dir}/casr1998/part101-manual-of-standards
    mkdir -p ${part101mos_parts_dir}

    pdftk ${download_dir}/casr1998/part101-manual-of-standards/casr1998-part101-manual-of-standards.pdf \
    cat \
    ${3}-${4} \
    output \
    ${part101mos_parts_dir}/${1}.pdf
    echo "Civil Aviation Safety Regulations 1998 Part 101 Manual of Standards,${2},${part101mos_parts_dir}/${1}.pdf" >> ${index_file}
  fi
}

extract_casr_1998_part101_mos_parts() {
  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter1" \
    "a. CHAPTER 1 Preliminary" \
    "9" \
    "18"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter2" \
    "b. CHAPTER 2 RePL Training Course" \
    "19" \
    "44"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter4" \
    "c. CHAPTER 4 Operations in Controlled Airspace - Controlled Aerodromes" \
    "46" \
    "49"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter5" \
    "d. CHAPTER 5 RPA Operations Beyond VLOS" \
    "50" \
    "57"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter9" \
    "e. CHAPTER 9 Operations of RPA in Prescribed Areas" \
    "59" \
    "64"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter10" \
    "f. CHAPTER 10 Record Keeping for Certain RPA" \
    "65" \
    "75"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter10a" \
    "g. CHAPTER 10A Significant Changes" \
    "76" \
    "77"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter10a" \
    "h. CHAPTER 11 Test Flights" \
    "78" \
    "79"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter12" \
    "i. CHAPTER 12 Identification of RPA and Model Aircraft" \
    "80" \
    "80"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter13" \
    "j. CHAPTER 13 Operation of Foreign Registered RPA and Model Aircraft" \
    "81" \
    "82"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter14" \
    "k. CHAPTER 14 Permissible Modifications to Registered RPA and Model Aircraft" \
    "83" \
    "83"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_chapter15" \
    "l. CHAPTER 15 Conduct of Online Training and Examinations for Accreditation" \
    "84" \
    "85"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_schedule1" \
    "m. Schedule 1 Acronyms and abbreviations" \
    "86" \
    "87"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_schedule2" \
    "n. Schedule 2 Directory for aeronautical knowledge standards for a RePL training course" \
    "88" \
    "89"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_schedule3" \
    "o. Schedule 3 Directory for practical competency standards for a RePL training course" \
    "90" \
    "91"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_schedule4" \
    "p. Schedule 4 Aeronautical knowledge units " \
    "92" \
    "121"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_schedule5" \
    "q. Schedule 5 Practical competency units" \
    "122" \
    "175"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_schedule6" \
    "r. Schedule 6 Flight Test Standards" \
    "176" \
    "205"

  extract_casr_1998_part101_mos \
    "casr1998-part101-manual-of-standards_note_to_part101" \
    "s. Note to Part 101 (Unmanned Aircraft and Rockets) Manual of Standards 2019" \
    "206" \
    "end"
}

extract_casr_1998Part61() {
  part61_dir=${extract_dir}/casr1998
  part61_file=${part61_dir}/casr1998-part61.pdf
  mkdir -p ${part61_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    88-330 \
    output \
    ${part61_file}

    echo "Civil Aviation Safety Regulations 1998,Part 61 Flight crew licensing,${part61_file}" >> ${index_file}
}

extract_casr_1998Part61_Subpart61O() {
  part61_subpart61o_dir=${extract_dir}/casr1998
  part61_subpart61o_file=${part61_subpart61o_dir}/casr1998-part61-subpart61o.pdf
  mkdir -p ${part61_subpart61o_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    240-245 \
    output \
    ${part61_subpart61o_file}

    echo "Civil Aviation Safety Regulations 1998,Part 61 Flight crew licensing Subpart 61.O NVFR Ratings,${part61_subpart61o_file}" >> ${index_file}
}

extract_casr_1998_part67() {
  part67_dir=${extract_dir}/casr1998
  part67_file=${part67_dir}/casr1998-part67.pdf
  mkdir -p ${part67_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    392-437 \
    output \
    ${part67_file}

    echo "Civil Aviation Safety Regulations 1998,Part 67 Medical,${part67_file}" >> ${index_file}
}

extract_casr_1998_part91() {
  part91_dir=${extract_dir}/casr1998
  part91_file=${part91_dir}/casr1998-part91.pdf
  mkdir -p ${part91_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    466-592 \
    output \
    ${part91_file}

    echo "Civil Aviation Safety Regulations 1998,Part 91 General Operating and Flight Rules,${part91_file}" >> ${index_file}
}

extract_casr_1998_part99() {
  part99_dir=${extract_dir}/casr1998
  part99_file=${part99_dir}/casr1998-part99.pdf
  mkdir -p ${part99_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    35-104 \
    output \
    ${part99_file}

    echo "Civil Aviation Safety Regulations 1998,Part 99 Drug and alcohol management plans and testing,${part99_file}" >> ${index_file}
}

extract_casr_1998_part101() {
  part101_dir=${extract_dir}/casr1998
  part101_file=${part101_dir}/casr1998-part101.pdf
  mkdir -p ${part101_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    105-176 \
    output \
    ${part101_file}

    echo "Civil Aviation Safety Regulations 1998,Part 101 Unmanned aircraft and rockets,${part101_file}" >> ${index_file}
}

extract_casr_1998_part103() {
  part103_dir=${extract_dir}/casr1998
  part103_file=${part103_dir}/casr1998-part103.pdf
  mkdir -p ${part103_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    177-193 \
    output \
    ${part103_file}

    echo "Civil Aviation Safety Regulations 1998,Part 103 Sport and recreation aircraft,${part103_file}" >> ${index_file}
}

extract_casr_1998_part121() {
  part121_dir=${extract_dir}/casr1998
  part121_file=${part121_dir}/casr1998-part121.pdf
  mkdir -p ${part121_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    264-354 \
    output \
    ${part121_file}

    echo "Civil Aviation Safety Regulations 1998,Part 121 Australian air transport operations-larger aeroplanes,${part121_file}" >> ${index_file}
}

extract_casr_1998_part135() {
  part135_dir=${extract_dir}/casr1998
  part135_file=${part135_dir}/casr1998-part135.pdf
  mkdir -p ${part135_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    522-579 \
    output \
    ${part135_file}

    echo "Civil Aviation Safety Regulations 1998,Part 135 Australian air transport operations-smaller aeroplanes,${part135_file}" >> ${index_file}
}

extract_casr_1998_part137() {
  part137_dir=${extract_dir}/casr1998
  part137_file=${part137_dir}/casr1998-part137.pdf
  mkdir -p ${part137_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    31-64 \
    output \
    ${part137_file}

    echo "Civil Aviation Safety Regulations 1998,Part 137 Aerial application operations-other than rotorcraft,${part137_file}" >> ${index_file}
}

extract_casr_1998_part138() {
  part138_dir=${extract_dir}/casr1998
  part138_file=${part138_dir}/casr1998-part138.pdf
  mkdir -p ${part138_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    65-131 \
    output \
    ${part138_file}

    echo "Civil Aviation Safety Regulations 1998,Part 138 Aerial work operations,${part138_file}" >> ${index_file}
}

extract_casr_1998_part141() {
  part141_dir=${extract_dir}/casr1998
  part141_file=${part141_dir}/casr1998-part141.pdf
  mkdir -p ${part141_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    182-215 \
    output \
    ${part141_file}

    echo "Civil Aviation Safety Regulations 1998,Part 141 Recreational private and commercial pilot flight training other than certain integrated training courses,${part141_file}" >> ${index_file}
}

extract_casr_1998_part142() {
  part142_dir=${extract_dir}/casr1998
  part142_file=${part142_dir}/casr1998-part142.pdf
  mkdir -p ${part142_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    216-265 \
    output \
    ${part142_file}

    echo "Civil Aviation Safety Regulations 1998,Part 142 Integrated and multi-crew pilot flight training contracted training and contracted checking,${part142_file}" >> ${index_file}
}

extract_cao_complete() {
  cao_complete_dir=${extract_dir}/cao
  cao_complete_file=${cao_complete_dir}/cao-complete.pdf
  mkdir -p ${cao_complete_dir}

  pdftk \
    ${download_dir}/cao/cao20.2.pdf \
    ${download_dir}/cao/cao20.3.pdf \
    ${download_dir}/cao/cao20.4.pdf \
    ${download_dir}/cao/cao20.6.pdf \
    ${download_dir}/cao/cao20.7.1.pdf \
    ${download_dir}/cao/cao20.7.1B.pdf \
    ${download_dir}/cao/cao20.7.2.pdf \
    ${download_dir}/cao/cao20.9.pdf \
    ${download_dir}/cao/cao20.10.pdf \
    ${download_dir}/cao/cao20.10.1.pdf \
    ${download_dir}/cao/cao20.11.pdf \
    ${download_dir}/cao/cao20.13.pdf \
    ${download_dir}/cao/cao20.16.1.pdf \
    ${download_dir}/cao/cao20.16.2.pdf \
    ${download_dir}/cao/cao20.16.3.pdf \
    ${download_dir}/cao/cao20.17.pdf \
    ${download_dir}/cao/cao20.18.pdf \
    ${download_dir}/cao/cao20.21.pdf \
    ${download_dir}/cao/cao20.22.pdf \
    ${download_dir}/cao/cao20.91.pdf \
    ${download_dir}/cao/cao29.2.pdf \
    ${download_dir}/cao/cao29.3.pdf \
    ${download_dir}/cao/cao29.4.pdf \
    ${download_dir}/cao/cao29.5.pdf \
    ${download_dir}/cao/cao29.6.pdf \
    ${download_dir}/cao/cao29.8.pdf \
    ${download_dir}/cao/cao29.10.pdf \
    ${download_dir}/cao/cao29.11.pdf \
    ${download_dir}/cao/cao40.2.2.pdf \
    ${download_dir}/cao/cao40.7.pdf \
    ${download_dir}/cao/cao45.0.pdf \
    ${download_dir}/cao/cao48.1.pdf \
    ${download_dir}/cao/cao82.0.pdf \
    ${download_dir}/cao/cao82.1.pdf \
    ${download_dir}/cao/cao82.7.pdf \
    ${download_dir}/cao/cao95.4.1.pdf \
    ${download_dir}/cao/cao95.7.pdf \
    ${download_dir}/cao/cao95.7.2.pdf \
    ${download_dir}/cao/cao95.7.3.pdf \
    ${download_dir}/cao/cao95.9.pdf \
    ${download_dir}/cao/cao95.14.pdf \
    ${download_dir}/cao/cao95.19.pdf \
    ${download_dir}/cao/cao95.20.pdf \
    ${download_dir}/cao/cao95.22.pdf \
    ${download_dir}/cao/cao95.23.pdf \
    ${download_dir}/cao/cao95.26.pdf \
    ${download_dir}/cao/cao95.27.pdf \
    ${download_dir}/cao/cao95.28.pdf \
    ${download_dir}/cao/cao95.29.pdf \
    ${download_dir}/cao/cao95.30.pdf \
    ${download_dir}/cao/cao95.31.pdf \
    ${download_dir}/cao/cao95.34.pdf \
    ${download_dir}/cao/cao95.55.pdf \
    ${download_dir}/cao/cao95.56.pdf \
    ${download_dir}/cao/cao95.57.pdf \
    cat \
    output \
    ${cao_complete_file}

    echo "Civil Aviation Order,Complete,${cao_complete_file}" >> ${index_file}
}

extract_cao_clwa() {
  cao_clwa_dir=${extract_dir}/clwa
  cao_clwa_file=${cao_clwa_dir}/cao.pdf
  mkdir -p ${cao_clwa_dir}

  pdftk \
    ${download_dir}/cao/cao20.2.pdf \
    ${download_dir}/cao/cao20.4.pdf \
    ${download_dir}/cao/cao20.7.2.pdf \
    ${download_dir}/cao/cao20.9.pdf \
    ${download_dir}/cao/cao20.10.pdf \
    ${download_dir}/cao/cao20.11.pdf \
    ${download_dir}/cao/cao20.16.1.pdf \
    ${download_dir}/cao/cao20.16.2.pdf \
    ${download_dir}/cao/cao20.16.3.pdf \
    ${download_dir}/cao/cao20.18.pdf \
    ${download_dir}/cao/cao29.5.pdf \
    ${download_dir}/cao/cao48.1.pdf \
    cat \
    output \
    ${cao_clwa_file}

    echo "CLWA Exam Extract,Civil Aviation Order,${cao_clwa_file}" >> ${index_file}
}

extract_rpl_cover() {
  rpl_dir=${extract_dir}/rpl
  rpl_cover_file=${rpl_dir}/rpl-exam-extract-cover.md
  mkdir -p ${rpl_dir}

  printf "# RPL Exam Legislation Extract\n\
\n\
## CASR 1998 Part 61 Flight crew licensing\n\
\n\
* 112 Flying as a student pilot\n\
* 113 General requirements for student pilots\n\
* 114 Solo flights—medical requirements for student pilots\n\
* 115 Solo flights—recent experience requirements for student pilots\n\
* 116 Student pilots authorised to taxi aircraft\n\
* 117 Identity checks—student pilots\n\
* 118 Production of medical certificates etc. and identification—student pilots\n\
* 119 Flying without licence—flight engineer duties\n\
* 120 Operation of aircraft radio without licence\n\
* 125 Conducting flight activities without rating or endorsement\n\
* 155 Applications for flight crew licences, ratings and endorsements\n\
* 160 Grant of flight crew licences\n\
* 165 Grant of flight crew ratings\n\
* 170 Grant of flight crew endorsements\n\
* 175 How CASA issues flight crew licences, ratings and endorsements\n\
* 375 Limitations on exercise of privileges of pilot licences—ratings\n\
* 380 Limitations on exercise of privileges of pilot licences—flight activity and design feature endorsements\n\
* 385 Limitations on exercise of privileges of pilot licences—general competency requirement\n\
* 390 Limitations on exercise of privileges of pilot licences—operating requirements and limitations\n\
* 395 Limitations on exercise of privileges of pilot licences—recent experience for certain passenger flight activities\n\
* 400 Limitations on exercise of privileges of pilot licences—flight review\n\
* 405 Limitations on exercise of privileges of pilot licences—medical requirements—recreational pilot licence holders\n\
* 410 Limitations on exercise of privileges of pilot licences—medical certificates: private pilot licence holders\n\
* 415 Limitations on exercise of privileges of pilot licences—medical certificates: commercial, multi-crew and air transport pilot licence holders\n\
* 420 Limitations on exercise of privileges of pilot licences—carriage of documents\n\
* 422 Limitations on exercise of privileges of pilot licences—aviation English language proficiency\n\
* 425 Limitations on exercise of privileges of pilot licences—unregistered aircraft\n\
* 427 Removal of certain pilot licence conditions about airspace\n\
* 430 Holders of pilot licences authorised to taxi aircraft\n\
* 435 When holders of pilot licences authorised to operate aircraft radio\n\
* 460 Privileges of recreational pilot licences\n\
* 465 Limitations on exercise of privileges of recreational pilot licences—general\n\
* 470 Limitations on exercise of privileges of recreational pilot licences—endorsements\n\
* 475 Requirements for grant of recreational pilot licences\n\
* 480 Grant of recreational pilot licences in recognition of pilot certificates granted by certain organisations\n\
* 485 Kinds of recreational pilot licence endorsements\n\
* 490 Privileges of recreational pilot licence endorsements\n\
* 495 Requirements for grant of recreational pilot licence endorsements\n\
* 500 Grant of endorsement in recognition of other qualifications\n\
* 505 Privileges of private pilot licences\n\
* 510 Limitations on exercise of privileges of private pilot licences—multi-crew operations\n\
* 515 Requirements for grant of private pilot licences—general\n\
* 755 Design features that require design feature endorsement\n\
* 760 Privileges of design feature endorsements\n\
* 765 Requirements for grant of design feature endorsements\n\
* 1227 Obligations of pilot instructors—approval to operate aircraft radio\n\
* 1230 Obligations of pilot instructors—records of activities conducted independently of Part 141 or 142 operator\n\
\n\
## CASR 1998 Part 67 Medical\n\
\n\
* 235 Suspension of medical certificates—pregnancy\n\
* 240 Medical certificates—suspension pending examination\n\
* 265 Obligation to tell CASA of changes in medical condition—medical certificate holders\n\
* 270 Offence—doing act while efficiency impaired—licence holders\n\
* 275 Surrender of medical certificates\n\
\n\
## CASR 1998 Part 91 General Operating and Flight Rules\n\
\n\
* 100 Electronic documents\n\
* 105 Carriage of documents\n\
* 110 Carriage of documents for certain flights\n\
* 115 Carriage of documents—flights that begin or end outside Australian territory\n\
* 120 Journey logs—flights that begin or end outside Australian territory\n\
* 160 Possessing firearm on aircraft\n\
* 165 Discharging firearm on aircraft\n\
* 273 VFR flights\n\
* 275 Specified VFR cruising levels\n\
* 277 Minimum heights—VFR flights at night\n\
* 280 VFR flights—compliance with VMC criteria\n\
* 283 VFR flights—aircraft not to exceed certain speeds\n\
* 285 VFR flights—flights in class A airspace\n\
* 287 IFR flights\n\
* 290 Specified IFR cruising levels\n\
* 295 IFR flights at non-specified cruising levels—notifying Air Traffic Services\n\
* 300 IFR flights at non-specified cruising levels—avoiding collisions with aircraft conducting VFR flights\n\
* 305 Minimum heights—IFR flights\n\
* 307 IFR take-off and landing minima\n\
* 310 Approach ban for IFR flights\n\
* 315 Taking off and landing in low visibility\n\
* 320 Specified aircraft performance categories\n\
* 325 Basic rule\n\
* 330 Right of way rules\n\
* 355 Giving way on water\n\
* 360 Meaning of in the vicinity of a non-controlled aerodrome\n\
* 365 Taxiing or towing on movement area of aerodrome\n\
* 370 Take-off or landing at non-controlled aerodrome—all aircraft\n\
* 375 Operating on manoeuvring area, or in the vicinity, of non-controlled aerodrome—general requirements\n\
* 380 Operating on manoeuvring area, or in the vicinity, of non-controlled aerodrome—landing and taking off into the wind\n\
* 385 Operating on manoeuvring area, or in the vicinity, of non-controlled aerodrome—requirements that apply after joining the circuit pattern\n\
* 390 Operating on manoeuvring area, or in the vicinity, of non-controlled aerodrome—requirements related to maintaining the same track after take-off\n\
* 395 Straight-in approaches at non-controlled aerodromes\n\
* 400 Communicating at certified, registered, military or designated non-controlled aerodromes\n\
* 405 Aircraft in aerodrome traffic at controlled aerodromes\n\
* 410 Use of aerodromes\n\
* 415 Taxiing aircraft\n\
* 420 Parked aircraft not to create hazard\n\
* 425 Safety when aeroplane operating on ground\n\
* 430 Safety when rotorcraft operating on ground\n\
* 455 Fuel requirements\n\
* 460 Oil requirements\n\
* 465 Contaminated, degraded or inappropriate fuels\n\
* 470 Fire hazards\n\
* 475 Fuelling aircraft—fire fighting equipment\n\
* 480 Fuelling aircraft—electrical bonding\n\
* 485 Equipment or electronic devices operating near aircraft\n\
* 490 Fuelling turbine-engine aircraft—low-risk electronic devices\n\
* 495 Only turbine-engine aircraft to be hot fuelled\n\
* 500 Hot fuelling aircraft—general\n\
* 505 Hot fuelling aircraft—procedures etc.\n\
* 510 Fuelling aircraft—persons on aircraft, boarding or disembarking\n\
* 515 Fuelling aircraft if fuel vapour detected\n\
* 520 Crew members to be fit for duty\n\
* 525 Offensive or disorderly behaviour on aircraft\n\
* 530 When smoking not permitted\n\
* 535 Crew safety during turbulence\n\
* 540 Means of passenger communication\n\
* 545 Seating for persons on aircraft\n\
* 550 Seating for flight crew members\n\
* 555 Seating for crew members other than flight crew members\n\
* 560 Restraint of infants and children\n\
* 565 Passengers—safety briefings and instructions\n\
* 570 Passengers—safety directions by pilot in command\n\
* 575 Passengers—compliance with safety directions\n\
* 580 Passengers—compliance with safety instructions by cabin crew\n\
* 585 Restraint and stowage of cargo\n\
* 590 Restraint and stowage of carry-on baggage\n\
* 595 Restraint and stowage of certain aircraft equipment\n\
* 600 Carriage of cargo—general\n\
* 605 Carriage of cargo—cargo compartments\n\
* 610 Carriage of cargo—unoccupied seats\n\
* 615 Carriage of cargo—loading instructions\n\
* 620 Carriage of animals\n\
* 625 Use of radio—qualifications\n\
* 630 Use of radio—broadcasts and reports\n\
* 635 Communication monitoring in controlled airspaces\n\
* 640 Use of radio outside controlled airspaces—listening watch of radio transmissions\n\
* 645 Availability of instructions for flight data and combination recorders\n\
* 650 Flight recorders—preserving recordings of immediately reportable matters\n\
* 655 RVSM airspace\n\
* 660 Performance-based navigation\n\
* 670 Standard visual signals\n\
* 675 Pilot in command to report hazards to air navigation\n\
* 680 Pilot in command to report emergencies\n\
* 685 Multi-engine aircraft—pilot in command to land at nearest suitable aerodrome if emergency occurs\n\
* 690 Pilot in command to report contraventions relating to emergencies\n\
* 695 Interception of aircraft\n\
* 700 Aviation distress signals\n\
* 705 Flight in icing conditions—adherence of frost, ice or snow\n\
* 710 Flight in icing conditions—requirements for flight\n\
* 795 Take-off performance\n\
* 800 Landing performance\n\
* 810 Requirements relating to equipment\n\
\n\
## Part 91 (General Operating and Flight Rules) Manual of Standards\n\
\n\
* CHAPTER 7 FLIGHT PREPARATION (WEATHER ASSESSMENTS) REQUIREMENTS\n\
* CHAPTER 8 FLIGHT PREPARATION (ALTERNATE AERODROMES) REQUIREMENTS\n\
* CHAPTER 10 MATTERS TO BE CHECKED BEFORE TAKE-OFF\n\
* CHAPTER 11 AIR TRAFFIC SERVICES — PRESCRIBED REQUIREMENTS\n\
* CHAPTER 12 MINIMUM HEIGHT RULES\n\
* CHAPTER 13 VFR FLIGHTS\n\
* CHAPTER 19 FUEL REQUIREMENTS\n\
* CHAPTER 20 SAFETY OF PERSONS AND CARGO ON AIRCRAFT\n\
* CHAPTER 21 RADIO FREQUENCY, BROADCAST AND REPORTING REQUIREMENTS\n\
* CHAPTER 23 INTERCEPTION OF AIRCRAFT\n\
* CHAPTER 24 TAKE-OFF PERFORMANCE\n\
* CHAPTER 25 LANDING PERFORMANCE\n\
* CHAPTER 26 EQUIPMENT\n\
  " > ${rpl_cover_file}

  rpl_cover_file_pdf=${rpl_dir}/rpl-exam-extract-cover.md.pdf
  pandoc ${rpl_cover_file} -V geometry:margin=0.4in -V mainfont="DejaVu Sans Mono" -o ${rpl_cover_file_pdf}
}

extract_rpl() {
  rpl_dir=${extract_dir}/rpl
  rpl_file=${rpl_dir}/rpl-exam-extract.pdf
  rpl_cover_file_pdf=${rpl_dir}/rpl-exam-extract-cover.md.pdf
  mkdir -p ${rpl_dir}

  pdftk \
    A=${download_dir}/casr1998/casr1998-volume2.pdf \
    B=${download_dir}/casr1998/part91-manual-of-standards/casr1998-part91-manual-of-standards.pdf \
    C=${rpl_cover_file_pdf} \
    cat \
    C \
    A120-124 A129-130 A155-171 A205-206 A282 \
    A429-430 A435-438 \
    A482-484 A490 A505-511 A515-548 A550-553 A564-565 A567 \
    B48-55 B58-75 B91-106 B108-166 \
    output \
    ${rpl_file}

    echo "RPL Exam Extract, CASR 1998 [Part 61 extract|Part 67 extract|Part 91 extract|Part 91 MOS extract],${rpl_file}" >> ${index_file}
}

extract_casr_1998_part61_pirc_cover() {
  part61_pirc_cover_dir=${extract_dir}/pirc
  part61_pirc_cover_file=${part61_pirc_cover_dir}/casr1998-part61.T-cover.md
  mkdir -p ${part61_pirc_cover_dir}

  printf "# Subpart 61.T—Pilot instructor ratings\n\
\n\
## Division 61.T.1—Privileges and requirements for grant of flight instructor ratings\n\
\n\
* 61.1165 Privileges of flight instructor ratings\n\
* 61.1170 Limitations on exercise of privileges of flight instructor ratings—general\n\
* 61.1175 Limitations on exercise of privileges of flight instructor ratings—endorsements\n\
* 61.1180 Limitations on exercise of privileges of flight instructor ratings—instructor proficiency check\n\
* 61.1185 Requirements for grant of flight instructor ratings\n\
\n\
## Division 61.T.2—Privileges and requirements for grant of simulator instructor ratings\n\
\n\
* 61.1190 Privileges of simulator instructor ratings\n\
* 61.1195 Limitations on exercise of privileges of simulator instructor ratings—general\n\
* 61.1200 Limitations on exercise of privileges of simulator instructor ratings—endorsements\n\
* 61.1205 Limitations on exercise of privileges of simulator instructor ratings—instructor proficiency check\n\
* 61.1210 Requirements for grant of simulator instructor ratings\n\
\n\
## Division 61.T.3—Obligations of pilot instructors\n\
\n\
* 61.1215 Obligations of pilot instructors—training\n\
* 61.1220 Obligations of pilot instructors—flight reviews\n\
* 61.1225 Obligations of pilot instructors—student pilots\n\
* 61.1227 Obligations of pilot instructors—approval to operate aircraft radio\n\
* 61.1230 Obligations of pilot instructors—records of activities conducted independently of Part 141 or 142 operator\n\
\n\
## Division 61.T.4—Privileges and requirements for grant of training endorsements\n\
\n\
* 61.1235 Kinds of training endorsement\n\
* 61.1240 Privileges of training endorsements\n\
* 61.1245 Limitations on exercise of privileges of training endorsements—general\n\
* 61.1246 Limitations on exercise of privileges of grade 3 training endorsements\n\
* 61.1247 Limitations on exercise of privileges of low level training endorsements\n\
* 61.1248 Limitation on exercise of privileges of helicopter grade 2 training endorsements\n\
* 61.1250 Requirements for grant of training endorsements\n\
  " > ${part61_pirc_cover_file}

  part61_pirc_cover_file_pdf=${rpl_dir}/casr1998-part61.T-cover.md.pdf
  pandoc ${part61_pirc_cover_file} -V geometry:margin=0.4in -V mainfont="DejaVu Sans Mono" -o ${part61_pirc_cover_file_pdf}
}

extract_casr_1998_part61_pirc() {
  part61_pirc_dir=${extract_dir}/pirc
  part61_pirc_file=${part61_pirc_dir}/casr1998-part61.T.pdf
  part61_pirc_cover_file_pdf=${rpl_dir}/casr1998-part61.T-cover.md.pdf
  mkdir -p ${part61_pirc_dir}

  pdftk \
    A=${download_dir}/casr1998/casr1998-volume2.pdf \
    B=${part61_pirc_cover_file_pdf} \
    cat \
    B \
    A269-289 \
    output \
    ${part61_pirc_file}

    echo "PIRC Exam Extract, Civil Aviation Safety Regulations 1998 Part 61,${part61_pirc_file}" >> ${index_file}
}

extract_casr_1998_part141_subpart_e() {
  part141_subpartE_dir=${extract_dir}/pirc
  part141_subpartE_file=${part141_subpartE_dir}/casr1998-part141-subpartE.pdf
  mkdir -p ${part141_subpartE_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    201-203 \
    output \
    ${part141_subpartE_file}

    echo "Part 141 operators-instructors, Civil Aviation Safety Regulations 1998 Part 141 Subpart 141.E,${part141_subpartE_file}" >> ${index_file}
}

decrypt_aip() {
  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/complete.pdf \
    ${download_dir}/aip/aip/complete_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Complete (decrypted),${download_dir}/aip/aip/complete_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/general.pdf \
    ${download_dir}/aip/aip/general_decrypted.pdf \

  echo "Aeronautical Information Publication Book,General (decrypted),${download_dir}/aip/aip/general_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/enroute.pdf \
    ${download_dir}/aip/aip/enroute_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Enroute (decrypted),${download_dir}/aip/aip/enroute_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/aerodrome.pdf \
    ${download_dir}/aip/aip/aerodrome_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Aerodrome (decrypted),${download_dir}/aip/aip/aerodrome_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/cover.pdf \
    ${download_dir}/aip/aip/cover_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Amendment Instructions (decrypted),${download_dir}/aip/aip/cover_decrypted.pdf" >> ${index_file}
}

decrypt_ersa() {
  qpdf \
    --decrypt \
    ${download_dir}/aip/ersa/ersa_rds_index.pdf \
    ${download_dir}/aip/ersa/ersa_rds_index_decrypted.pdf \

  echo "Aeronautical Information Publication En Route Supplement Australia,Complete (decrypted),${download_dir}/aip/ersa/ersa_rds_index_decrypted.pdf" >> ${index_file}
}

extract_ersa() {
  if [ $# -lt 4 ]; then
    echo "arguments(4) <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    ersa_parts_dir=${extract_dir}/aip/ersa
    mkdir -p ${ersa_parts_dir}

    pdftk \
      ${download_dir}/aip/ersa/ersa_rds_index_decrypted.pdf \
      cat \
      ${3}-${4} \
      output \
      ${ersa_parts_dir}/${1}.pdf

      echo "Aeronautical Information Publication En Route Supplement Australia,${2},${ersa_parts_dir}/${1}.pdf" >> ${index_file}
  fi
}

extract_ersa_parts() {
  extract_ersa \
    "ersa_fac_index" \
    "a. FAC - INDEX OF AERODROMES" \
    "5" \
    "10"

  extract_ersa \
    "ersa_avfax" \
    "b. AVFAX - AVFAX MADE SIMPLE" \
    "11" \
    "12"

  extract_ersa \
    "ersa_intro" \
    "c. INTRO - ERSA INTRODUCTION" \
    "13" \
    "34"

  extract_ersa \
    "ersa_fac" \
    "d. FACILITIES" \
    "35" \
    "831"

  extract_ersa \
    "ersa_prd_areas" \
    "e. SUA" \
    "833" \
    "846"

  extract_ersa \
    "ersa_aerodrome_ala_codes" \
    "f. AERODROME AND ALA CODES" \
    "847" \
    "901"

  extract_ersa \
    "ersa_ifr_waypoints" \
    "g. IFR WAYPOINTS" \
    "903" \
    "927"

  extract_ersa \
    "ersa_vfr_waypoints" \
    "h. VFR WAYPOINTS" \
    "929" \
    "956"

  extract_ersa \
    "ersa_gen_flight_plan_requirements" \
    "i. GEN - FPR - FLIGHT PLAN REQUIREMENTS" \
    "957" \
    "1006"

  extract_ersa \
    "ersa_gen_special_procedures" \
    "j. GEN - SP - SPECIAL PROCEDURES" \
    "1007" \
    "1034"

  extract_ersa \
    "ersa_fis_preflight" \
    "k. GEN - FIS - INFLIGHT" \
    "1035" \
    "1041"

  extract_ersa \
    "ersa_nav_comm" \
    "l. NAV/COMM - NAVIGATION AND COMMUNICATION" \
    "1043" \
    "1045"

  extract_ersa \
    "ersa_emergency_procedures" \
    "r. EMERG - EMERGENCY PROCEDURES" \
    "1047" \
    "1069"

  extract_ersa \
    "ersa_rds" \
    "s. RDS - RUNWAY DISTANCE SUPPLEMENT" \
    "1071" \
    "1193"

  extract_ersa \
    "ersa_index" \
    "t. INDEX" \
    "1195" \
    "1198"
}

mkdir -p ${download_dir}

extract_car_1988_complete
extract_casr_1998_complete
extract_casr_1998Part61
extract_casr_1998Part61_Subpart61O
extract_casr_1998_part67
extract_casr_1998_part91
extract_casr_1998_part99
extract_casr_1998_part101
extract_casr_1998_part103
extract_casr_1998_part121
extract_casr_1998_part135
extract_casr_1998_part137
extract_casr_1998_part138
extract_casr_1998_part141
extract_casr_1998_part142
extract_casr_1998_part61_mos_complete
extract_casr_1998_part61_mos_parts
extract_casr_1998_part91_mos_parts
extract_casr_1998_part101_mos_parts
extract_cao_complete
extract_cao_clwa
extract_rpl_cover
extract_rpl
extract_casr_1998_part61_pirc_cover
extract_casr_1998_part61_pirc
extract_casr_1998_part141_subpart_e
decrypt_aip
decrypt_ersa
extract_ersa_parts

#!/bin/bash

set -e
set -x

if ! type pdfinfo >/dev/null ; then
  >&2 echo "Missing: pdfinfo" >&2
  exit 1
fi

if [ -z "$1" ]
then
  >&2 echo "arguments: <download-dir>"
  exit 33
else
  download_dir=${1}
fi

make_page1() {
  pdf_files=$(cd ${download_dir} > /dev/null && find . -type f -name "*.pdf")

  OIFS=$IFS
  IFS=$'\n'
  for pdf_file in $pdf_files
  do
    basename=$(basename -- "${pdf_file}")
    dirname=$(dirname -- "${pdf_file}")
    filename="${basename%.*}"

    page1_pdf_dir=${download_dir}/${dirname}
    page1_pdf="${page1_pdf_dir}/${filename}_page1.pdf"

    mkdir -p "${page1_pdf_dir}"

    gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${download_dir}/${pdf_file}

    page1_pdf_png="${page1_pdf}.png"

    convert ${page1_pdf} ${page1_pdf_png}

    for size in 600 450 350 250 150 100 65 45
    do
      page1_pdf_png_size="${page1_pdf}-${size}.png"
      convert ${page1_pdf_png} -resize ${size}x${size} ${page1_pdf_png_size}
    done
  done
  IFS="$OIFS"
}

archive_n_pages() {
  pushd ${download_dir}
  find -L . -type f -name "*.npages" | sort | xargs -d "\n" tar -cvzf npages.tar.gz
  popd
}

n_pages() {
  if [ -z "$1" ]
  then
    >&2 echo "Require one (1) argument <input-pdf>" >&2
  else
    pdfinfo "${1}" | awk '/^Pages:/ {print $2}' | tr -d '[:space:]' > "${1}.npages"
  fi
}

make_n_pages() {
  pdf_files=$(cd ${download_dir} > /dev/null && find . -type f -name "*.pdf")

  pushd "${download_dir}"
  OIFS=$IFS
  IFS=$'\n'
  for pdf_file in $pdf_files
  do
    n_pages "${pdf_file}"
  done
  IFS="$OIFS"
  popd
}

mkdir -p ${download_dir}

make_page1
make_n_pages
archive_n_pages

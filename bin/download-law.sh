#!/bin/bash

set -e
set -x

if ! type wget >/dev/null ; then
  >&2 echo "Missing: wget" >&2
  exit 1
fi

if ! type qpdf >/dev/null ; then
  >&2 echo "Missing: qpdf" >&2
  exit 1
fi

if [ -z "$3" ]
then
  >&2 echo "arguments: <download-dir> <index-file> <aip-ersa-date> <aip-book-date>"
  exit 33
else
  download_dir=${1}
  index_file=${2}
  aip_ersa=${3}
  aip_book=${3}
fi

download_legislation() {
  if [ $# -lt 6 ]; then
    echo "arguments(6) <legislation-dir> <legislation-path> <out-dir> <out-file> <header> <name>"
    exit 9
  else
    out_dir=${download_dir}/${3}
    out_file=${out_dir}/${4}
    mkdir -p ${out_dir}
    wget -e robots=off --retry-on-http-error=503 https://www.legislation.gov.au/${1}/${2} -O ${out_file}
    # wget --ciphers DEFAULT@SECLEVEL=1 -e robots=off https://www.legislation.gov.au/Details/${1}/${2} -O ${out_file}
    qpdf --check ${out_file}
    if [ "$?" != 0 ]; then
      >&2 echo "PDF file failed check ${out_file}"
      exit 13
    fi

    echo "${5},${6},${out_file}" >> ${index_file}
  fi
}

download_all_legislation() {
  # CAR 1988
  download_legislation "F1997B00935" "2024-04-11/2024-04-11/text/original/pdf/1" car1988 car1988-volume1.pdf "Civil Aviation Regulations 1988" "Volume 1"
  download_legislation "F1997B00935" "2024-04-11/2024-04-11/text/original/pdf/2" car1988 car1988-volume2.pdf "Civil Aviation Regulations 1988" "Volume 2"

  # CASR 1998
  download_legislation "F1998B00220" "2024-04-11/2024-04-11/text/original/pdf/1" casr1998 casr1998-volume1.pdf "Civil Aviation Safety Regulations 1998" "Volume 1"
  download_legislation "F1998B00220" "2024-04-11/2024-04-11/text/original/pdf/2" casr1998 casr1998-volume2.pdf "Civil Aviation Safety Regulations 1998" "Volume 2"
  download_legislation "F1998B00220" "2024-04-11/2024-04-11/text/original/pdf/3" casr1998 casr1998-volume3.pdf "Civil Aviation Safety Regulations 1998" "Volume 3"
  download_legislation "F1998B00220" "2024-04-11/2024-04-11/text/original/pdf/4" casr1998 casr1998-volume4.pdf "Civil Aviation Safety Regulations 1998" "Volume 4"
  download_legislation "F1998B00220" "2024-04-11/2024-04-11/text/original/pdf/5" casr1998 casr1998-volume5.pdf "Civil Aviation Safety Regulations 1998" "Volume 5"

  # Part 61 Manual of Standards
  download_legislation "F2014L01102" "2021-05-18/2021-05-18/text/1/pdf/1" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume1.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 1"
  download_legislation "F2014L01102" "2021-05-18/2021-05-18/text/1/pdf/2" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume2.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 2"
  download_legislation "F2014L01102" "2021-05-18/2021-05-18/text/1/pdf/3" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume3.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 3"
  download_legislation "F2014L01102" "2021-05-18/2021-05-18/text/1/pdf/4" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume4.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 4"

  # Part 101 Manual of Standards
  download_legislation "F2019L00593" "2024-04-30/2024-04-30/text/original/pdf" casr1998/part101-manual-of-standards casr1998-part101-manual-of-standards.pdf "Civil Aviation Safety Regulations 1998 Part 101 Manual of Standards" "Unmanned Aircraft and Rockets Manual of Standards 2019 (as amended)"

  # Part 91 Manual of Standards
  download_legislation "F2020L01514" "2024-02-10/2024-02-10/text/original/pdf" casr1998/part91-manual-of-standards casr1998-part91-manual-of-standards.pdf "Civil Aviation Safety Regulations 1998 Part 91 Manual of Standards" "Complete"

  # # CAO
  download_legislation "F2005B00775" "2006-05-16/2006-05-16/text/original/pdf/1" cao cao20.2.pdf "Civil Aviation Order" "20.2 Air service operations - safety precautions before flight"
  download_legislation "F2005B00776" "asmade/2004-12-02/text/original/pdf" cao cao20.3.pdf "Civil Aviation Order" "20.3 Air service operations - marshalling and parking of aircraft"
  download_legislation "F2005B00777" "asmade/2004-12-02/text/original/pdf" cao cao20.4.pdf "Civil Aviation Order" "20.4 Provision and use of oxygen and protective breathing equipment"
  download_legislation "F2005B00778" "2010-08-07/2010-08-07/text/original/pdf/1" cao cao20.6.pdf "Civil Aviation Order" "20.6 Continuation of flight with 1 or more engines inoperative"
  download_legislation "F2005B00781" "asmade/2004-12-02/text/original/pdf/1" cao cao20.7.1.pdf "Civil Aviation Order" "20.7.1 Aeroplane weight limitations - Aeroplanes above 5 700 kg - all operations (piston-engined)"
  download_legislation "F2005B00782" "2014-05-24/2014-05-24/text/original/pdf" cao cao20.7.1B.pdf "Civil Aviation Order" "20.7.1B Aeroplane weight and performance limitations - specified aeroplanes above 5 700 kg or 2 722 kg if driven by 2 or more jet engines - all operations as amended"
  download_legislation "F2005B00785" "asmade/2004-12-02/text/original/pdf/1" cao cao20.7.2.pdf "Civil Aviation Order" "20.7.2 Aeroplane weight and performance limitations - aeroplanes not above 5 700 kg - regular public transport operations"
  download_legislation "F2005B00787" "2011-11-10/2011-11-10/text/original/pdf/1" cao cao20.9.pdf "Civil Aviation Order" "20.9 Air service operations - precautions in refuelling engine and ground radar operations"
  download_legislation "F2005B00788" "asmade/2004-12-02/text/original/pdf" cao cao20.10.pdf "Civil Aviation Order" "20.10 Hot Refuelling - helicopters"
  download_legislation "F2005B00790" "asmade/2004-12-02/text/original/pdf/1" cao cao20.10.1.pdf "Civil Aviation Order" "20.10.1 Hot Refuelling - turbine engine aeroplane engaged in aerial work or private operations"
  download_legislation "F2005B00791" "2009-02-01/2009-02-01/text/1/pdf/1" cao cao20.11.pdf "Civil Aviation Order" "20.11 Emergency and life saving equipment and passenger control in emergencies"
  download_legislation "F2005B00792" "asmade/2004-12-02/text/original/pdf/1" cao cao20.13.pdf "Civil Aviation Order" "20.13 Air service operations - cockpit check systems"
  download_legislation "F2005B00794" "2018-06-30/2018-06-30/text/original/pdf" cao cao20.16.1.pdf "Civil Aviation Order" "20.16.1 Air service operations - loading - general as amended"
  download_legislation "F2005B00989" "asmade/2004-12-02/text/original/pdf/1" cao cao20.16.2.pdf "Civil Aviation Order" "20.16.2 Air service operations - loading - general"
  download_legislation "F2005B00797" "2021-07-27/2021-07-27/text/original/pdf" cao cao20.16.3.pdf "Civil Aviation Order" "20.16.3 Air service operations - carriage of persons"
  download_legislation "F2005B00799" "asmade/2004-12-02/text/original/pdf" cao cao20.17.pdf "Civil Aviation Order" "20.17 Air service Operations - use of military aerodromes by civil aircraft"
  download_legislation "F2014L01743" "2020-06-11/2020-06-11/text/original/pdf" cao cao20.18.pdf "Civil Aviation Order" "20.18 Aircraft equipment - basic operational requirements"
  download_legislation "F2015L00662" "asmade/2015-05-08/text/original/pdf" cao cao20.21.pdf "Civil Aviation Order" "20.21"
  download_legislation "F2005B00807" "asmade/2004-12-02/text/original/pdf" cao cao20.22.pdf "Civil Aviation Order" "20.22 Taxiing of aircraft by persons other than licensed pilots"
  download_legislation "F2014L01703" "2018-11-02/2018-11-02/text/original/pdf" cao cao20.91.pdf "Civil Aviation Order" "20.91 Instructions and directions for performance-based navigation"
  download_legislation "F2005B00820" "2014-09-01/2014-09-01/text/original/pdf" cao cao29.2.pdf "Civil Aviation Order" "29.2 Air service operations - night flying training"
  download_legislation "F2005B00825" "asmade/2004-12-02/text/original/pdf" cao cao29.3.pdf "Civil Aviation Order" "29.3 Air service operations - aeroplanes engaged in agricultural operation - night aerial spraying"
  download_legislation "F2005B00835" "2014-09-01/2014-09-01/text/original/pdf" cao cao29.4.pdf "Civil Aviation Order" "29.4 Air Displays"
  download_legislation "F2005B00836" "2014-09-01/2014-09-01/text/original/pdf" cao cao29.5.pdf "Civil Aviation Order" "29.5 Air service operations - miscellaneous dropping of articles from aircraft in flight"
  download_legislation "F2005B00837" "2014-09-01/2014-09-01/text/original/pdf" cao cao29.6.pdf "Civil Aviation Order" "29.6 Air service operations - helicopter external sling load operations"
  download_legislation "F2005B00838" "asmade/2004-12-02/text/original/pdf" cao cao29.8.pdf "Civil Aviation Order" "29.8 Ferry flight of aeroplanes with 1 engine inoperative"
  download_legislation "F2005B00839" "/2014-09-01/2014-09-01/text/original/pdf" cao cao29.10.pdf "Civil Aviation Order" "29.10 Air service operations - aircraft engaged in aerial stock mustering operations - low flying permission"
  download_legislation "F2005B00842" "2014-09-01/2014-09-01/text/original/pdf" cao cao29.11.pdf "Civil Aviation Order" "29.11 Air service operations - helicopter winching and rappelling operations"
  download_legislation "F2005B00858" "2014-09-01/2014-09-01/text/original/pdf" cao cao40.2.2.pdf "Civil Aviation Order" "40.2.2 Balloon grade of night V.F.R. rating"
  download_legislation "F2005B00871" "2019-09-01/2019-09-01/text/original/pdf" cao cao40.7.pdf "Civil Aviation Order" "40.7 Aircraft endorsements (balloons) and flight instructor (balloons) ratings as amended"
  download_legislation "F2005B00648" "asmade/2004-03-18/text/original/pdf" cao cao45.0.pdf "Civil Aviation Order" "45.0 Flight crew standards - synthetic trainers - general"
  download_legislation "F2019L01070" "2021-12-02/2021-12-02/text/original/pdf" cao cao48.1.pdf "Civil Aviation Order" "48.1"
  download_legislation "F2014L01689" "2021-12-02/2021-12-02/text/original/pdf" cao cao82.0.pdf "Civil Aviation Order" "82.0"
  download_legislation "F2005B00881" "2021-12-02/2021-12-02/text/original/pdf" cao cao82.1.pdf "Civil Aviation Order" "82.1 Conditions on Air Operators' Certificates authorising charter operations and aerial work operations"
  download_legislation "F2005B00884" "2021-12-02/2021-12-02/text/original/pdf" cao cao82.7.pdf "Civil Aviation Order" "82.7 Air Operators' Certificates authorising aerial work operations and charter operations in balloons"
  download_legislation "F2005B00888" "2013-03-06/2013-03-06/text/original/pdf" cao cao95.4.1.pdf "Civil Aviation Order" "95.4.1 Exemption from provisions of the Civil Aviation Regulations 1988 - gliders engaged in charter operations"
  download_legislation "F2005B00889" "2010-10-20/2010-10-20/text/original/pdf/1" cao cao95.7.pdf "Civil Aviation Order" "95.7 Exemption from provisions of the Civil Aviation Regulations 1988 - helicopters"
  download_legislation "F2005B00890" "asmade/2004-12-12/text/original/pdf/1" cao cao95.7.2.pdf "Civil Aviation Order" "95.7.2 Exemption of certain helicopters engaged in rappelling sling load or winching operations from compliance with certain flight manual limitations"
  download_legislation "F2005B00892" "asmade/2004-12-12/text/original/pdf/1" cao cao95.7.3.pdf "Civil Aviation Order" "95.7.3 Exemption of certain helicopters engaged in transfer-ring marine pilots from compliance with subregulation 174B (2) of the Civil Aviation Regulations 1988"
  download_legislation "F2005B00894" "asmade/2004-12-12/text/original/pdf/1" cao cao95.9.pdf "Civil Aviation Order" "95.9 Exemption of Australian aeroplanes from compliance with certain provisions of the Civil Aviation Regulations 1988 - demonstration flights outside Australia"
  download_legislation "F2005B00901" "2014-09-01/2014-09-01/text/original/pdf" cao cao95.14.pdf "Civil Aviation Order" "95.14 Exemption from provisions of the regulations under the Civil Aviation Act 1988 - parasails and gyrogliders"
  download_legislation "F2005B00902" "asmade/2004-12-12/text/original/pdf" cao cao95.19.pdf "Civil Aviation Order" "95.19 Exemption from provisions of the Civil Aviation Regulations 1988 - F/A 18 aircraft"
  download_legislation "F2005B00903" "asmade/2004-12-12/text/original/pdf" cao cao95.20.pdf "Civil Aviation Order" "95.20 Exemption from provisions of the Civil Aviation Regulations 1988 - operation of military (state) aircraft by civilian flight crew"
  download_legislation "F2005B00905" "asmade/2004-12-12/text/original/pdf" cao cao95.22.pdf "Civil Aviation Order" "95.22 Exemption from provisions of the Civil Aviation Regulations 1988 - float planes operating in prescribed access lanes"
  download_legislation "F2005B00906" "asmade/2004-12-12/text/original/pdf/1" cao cao95.23.pdf "Civil Aviation Order" "95.23 Exemption from subregulations 178(1) and (2) of the Civil Aviation Regulations 1988 - for offshore and coastal surveillance and search and rescue"
  download_legislation "F2005B00908" "asmade/2004-12-12/text/original/pdf/1" cao cao95.26.pdf "Civil Aviation Order" "95.26 Exemption from subregulations 178(1) and (2) of the Civil Aviation Regulations 1988 - for trial operations of supply dropping of search and rescue (SAR) stores at night"
  download_legislation "F2005B00910" "asmade/2004-12-12/text/original/pdf/1" cao cao95.27.pdf "Civil Aviation Order" "95.27 Exemption from provisions of the Civil Aviation Regulations 1988 - S-70A-9 helicopter"
  download_legislation "F2005B00911" "asmade/2004-12-12/text/original/pdf" cao cao95.28.pdf "Civil Aviation Order" "95.28 Exemption from provisions of the Civil Aviation Regulations 1988 - S-70B-2 helicopter"
  download_legislation "F2005B00913" "asmade/2004-12-12/text/original/pdf" cao cao95.29.pdf "Civil Aviation Order" "95.29 Exemption from provisions of the Civil Aviation Regulations 1988 - Pilatus PC9 aircraft"
  download_legislation "F2005B00917" "asmade/2004-12-12/text/original/pdf/1" cao cao95.30.pdf "Civil Aviation Order" "95.30 Exemption from provisions of the Civil Aviation Regulations 1988 and the Civil Aviation Safety Regulations 1998 - British Aerospace MK 127 aircraft"
  download_legislation "F2005B00918" "asmade/2004-12-12/text/original/pdf" cao cao95.31.pdf "Civil Aviation Order" "95.31 Exemption from provisions of the Civil Aviation Regulations 1988 and the Civil Aviation Safety Regulations 1998 - Kaman Super Seasprite SH-2G(A) aircraft"
  download_legislation "F2007L04300" "asmade/2007-11-13/text/original/pdf" cao cao95.34.pdf "Civil Aviation Order" "95.34 NHIndustries NH90 Tactical Transport Helicopter"
  download_legislation "F2024L00264" "asmade/2024-03-01/text/original/pdf" cao cao95.55.pdf "Civil Aviation Order" "95.55 Exemptions from CAR and CASR - Certain Light Sport Aircraft Lightweight Aeroplanes and Ultralight Aeroplanes) Instrument 2021"
  download_legislation "F2006L00838" "asmade/2006-03-16/text/original/pdf" cao cao95.56.pdf "Civil Aviation Order" "95.56 Exemption from provisions of the Civil Aviation Regulations 1988 - light sport aircraft"
  download_legislation "F2008L02849" "asmade/2008-08-19/text/original/pdf" cao cao95.57.pdf "Civil Aviation Order" "95.57 EADS CASA Airbus A330 multi-role transport tanker"
}

download_aip() {
  if [ $# -lt 6 ]; then
    echo "arguments(6) <aip-dir> <aip-file> <out-dir> <out-file> <header> <name>"
    exit 8
  else
      out_dir=${download_dir}/${3}
      out_file=${out_dir}/${4}
      mkdir -p ${out_dir}
      wget -e robots=off https://www.airservicesaustralia.com/aip/current/${1}/${2} -O ${out_file}
      echo "${5},${6},${out_file}" >> ${index_file}
  fi
}

download_all_aip() {
  download_aip "aip" "complete_${aip_book}.pdf" aip/aip complete.pdf "Aeronautical Information Publication Book" "Complete"
  download_aip "aip" "general_${aip_book}.pdf" aip/aip general.pdf "Aeronautical Information Publication Book" "General"
  download_aip "aip" "enroute_${aip_book}.pdf" aip/aip enroute.pdf "Aeronautical Information Publication Book" "Enroute"
  download_aip "aip" "aerodrome_${aip_book}.pdf" aip/aip aerodrome.pdf "Aeronautical Information Publication Book" "Aerodrome"
  download_aip "aip" "cover_${aip_book}.pdf" aip/aip cover.pdf "Aeronautical Information Publication Book" "Amendment Instructions"
  download_aip "ersa" "ersa_rds_index_${aip_ersa}.pdf" aip/ersa ersa_rds_index.pdf "Aeronautical Information Publication En Route Supplement Australia" "Complete"
}

download_advisory_circular() {
  if [ $# -lt 3 ]; then
    echo "arguments(3) <ac-file> <out-file> <name>"
    exit 8
  else
      out_dir=${download_dir}/advisory-circular
      out_file=${out_dir}/${2}
      mkdir -p ${out_dir}
      wget -e robots=off https://www.casa.gov.au/${1} -O ${out_file}
      echo "Advisory Circular,${3},${out_file}" >> ${index_file}
  fi
}

download_all_advisory_circular() {
  download_advisory_circular "flight-instructor-training" ac-61-07.pdf "AC 61-07 v1.0 Flight instructor training [Replaces CAAP 5.14-2]"
  download_advisory_circular "pilots-responsibility-collision-avoidance" ac-91-14.pdf "AC 91-14 v1.0 Pilots' responsibility for collision avoidance [Replaces CAAP 166-2(1)]"
  download_advisory_circular "restraints-infants-and-children" ac-91-18.pdf "AC 91-18 v1.1 Restraint of infants and children [Replaces CAAP 235-2(2)]"
  download_advisory_circular "upset-prevention-and-recovery-training" ac-121-03.pdf "AC 121-03 v1.0 Upset prevention and recovery training"
  download_advisory_circular "carriage-assistance-animals" ac-91-03.pdf "AC 91-03 v1.0 Carriage of assistance animals"
  download_advisory_circular "guidelines-aeroplanes-mtow-not-exceeding-5-700-kg-suitable-places-take-and-land" ac-91-02.pdf "AC 91-02 v1.1 Guidelines for aeroplanes with MTOW not exceeding 5 700 kg - suitable places to take off and land [Supports regulation 91.410 of CASR]"
  download_advisory_circular "wake-turbulence" ac-91-16.pdf "AC 91-16 v1.0 Wake turbulence"
  download_advisory_circular "aircraft-checklists" ac-91-22.pdf "AC 91-22 v2.0 Aircraft checklists"
  download_advisory_circular "passenger-safety-information" ac-91-19_121-04_133-10_135-12_138-10.pdf "AC 91-19 | 121-04 | 133-10 | 135-12 | 138-10 Passenger safety information [Replaces CAAP 253-2(2)]"
  download_advisory_circular "electronic-flight-bags-ac" ac-91-17.pdf "AC 91-17 v1.0 Electronic flight bags [Replaces CAAP 233-1(1)]"
  download_advisory_circular "fuel-and-oil-safety" ac-91-25.pdf "AC 91-25 v1.0 Fuel and oil safety"
  download_advisory_circular "download/ditching-ac" ac-91-09.pdf "AC 91-09 v1.0 Ditching [Replaces CAAP 253-1(1)]"
  download_advisory_circular "operations-vicinity-non-controlled-aerodromes" ac-91-10.pdf "AC 91-10 Operations in the vicinity of non-controlled aerodromes [Replaces CAAP 166-01]"
  download_advisory_circular "guidelines-aircraft-fuel-requirements" ac-91-15.pdf "AC 91-15 Guidelines for aircraft fuel requirements [Replaces CAAP 234-1]"
}

# https://www.casa.gov.au/sites/default/files/2021-08/caap-48-01-fatigue-management-for-flight-crew-members.pdf
#   CAAP 48-01 Fatigue management for flight crew members
download_caap() {
  if [ $# -lt 3 ]; then
    echo "arguments(3) <caap-file> <out-file> <name>"
    exit 8
  else
      out_dir=${download_dir}/caap
      out_file=${out_dir}/${2}
      mkdir -p ${out_dir}
      wget -e robots=off https://www.casa.gov.au/${1} -O ${out_file}
      echo "Civil Aviation Advisory Publication,${3},${out_file}" >> ${index_file}
  fi
}

download_all_caap() {
  download_caap "sites/default/files/2021-08/caap-48-01-fatigue-management-for-flight-crew-members.pdf" caap-48-01.pdf "CAAP 48-01 Fatigue management for flight crew members"
}

mkdir -p ${download_dir}
rm -f ${index_file}

download_all_legislation
download_all_aip
# download_all_advisory_circular
# download_all_caap

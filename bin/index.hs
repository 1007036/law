#!/usr/bin/env cabal

{- cabal:
build-depends:
  alignment >= 0.1.0.3,
  base >= 4.12,
  containers >= 0.6 && < 1,
  filepath >= 1.4 && < 2,
  lens >= 4.15 && < 6
-}

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE LambdaCase #-}

module Main where

import Control.Applicative( Applicative(liftA2) )
import Control.Lens hiding (Index, each)
import Control.Monad( ap )
import Data.Alignment
import Data.Char ( isDigit, toLower )
import Data.Function ( on )
import Data.Maybe ( fromMaybe, mapMaybe )
import Data.List.NonEmpty ( groupBy, sortBy, toList, NonEmpty(..) )
import Data.Map(Map)
import qualified Data.Map.Strict as Map
import System.FilePath ( makeRelative )
import System.Environment ( getArgs )
import System.Exit ( exitWith, ExitCode(ExitFailure) )
import System.IO ( stderr, hPutStrLn )

main ::
  IO ()
main =
  do  a <- getArgs
      case a of
        (lk:inF:outF:hostLink:srcHost:lastTime:lastUsername:lastUserEmail:rev:access:_) ->
          run
            lk
            inF
            outF
            (DocumentMetadata hostLink srcHost lastTime lastUsername lastUserEmail rev access)
        _ ->
          do  hPutStrLn stderr "arguments <relative-link> <in-file> <out-file> <host-link> <source-host> <last-updated-time> <last-updated-username> <last-updated-user-email> <revision> <access-control>"
              exitWith (ExitFailure 4)

run ::
  String -- relative link
  -> FilePath -- in file
  -> FilePath -- out file
  -> DocumentMetadata
  -> IO ()
run lk inF outF d =
  do  f <- readFile inF
      case parse (render "  " "\n" id . printIndexDocument (makeRelative lk) d <$> parseIndex) f of
        ParseError e ->
          do  hPutStrLn stderr ("parse error " <> e)
              exitWith (ExitFailure 7)
        ParseSuccess r@(_:_) _ ->
          do  hPutStrLn stderr ("remaining parse input " <> r)
              exitWith (ExitFailure 6)
        ParseSuccess [] a ->
          writeFile outF a

data IndexValue =
  IndexValue
    (NonEmpty Char)
    (NonEmpty Char)
  deriving (Eq, Ord, Show)

indexName ::
  IndexValue
  -> NonEmpty Char
indexName (IndexValue n _) =
  n

indexRef ::
  IndexValue
  -> NonEmpty Char
indexRef (IndexValue _ r) =
  r

newtype Index =
  Index
    (Map (NonEmpty Char) (NonEmpty IndexValue))
  deriving (Eq, Ord, Show)

makeIndex ::
  [(NonEmpty Char, IndexValue)]
  -> Index
makeIndex =
  Index . makeMap

makeMap ::
  Ord a =>
  [(a, b)]
  -> Map a (NonEmpty b)
makeMap =
  foldl (\m (a, b) -> Map.insertWith (<>) a (pure b) m) Map.empty

----

printIndexValue ::
  (FilePath -> FilePath)
  -> IndexValue
  -> Doc String
printIndexValue lk i =
  tag "span" [] (pure (toList (indexName i))) <>
  tag "span" [("class", "float-right")] (inlineTag' (\h -> pure ("[" <> h)) (\h -> pure (h <> "]")) "a" [("href", lk (toList (indexRef i)))] (pure "pdf")) <>
  pure (openingTag "br" [])

printIndexValueList ::
  (FilePath -> FilePath)
  -> NonEmpty IndexValue
  -> Doc String
printIndexValueList lk i =
  let i' =
        sortBy (on (sortPriorityWords ["Complete", "Complete (decrypted)", "Contents"]) (toList . indexName)) i
  in  tag "ul" [("class", "list-group")] $
      foldMap (tag "li" [("class", "list-group-item")] . printIndexValue lk) i'

printIndex ::
  (FilePath -> FilePath)
  ->  Index
  -> Doc String
printIndex lk (Index i) =
  let i' =
        Map.toList i
      each h v =
        let h' =
              toList h
            a' =
              anchorId h'
        in  tag "div" [("class", "text-left"), ("style", "font-size: 14px")] (
              tag "a" [("href", "#" <> a')] (pure "#")
            ) <>
            tag "div" [("id", a'), ("class", "section-header text-left")] (
              tag "h3" [("class", "bottom-line")] (pure h')
            ) <>
            printIndexValueList lk v <>
            pure (openingTag "br" []) <>
            pure (openingTag "br" [])
  in  foldMap (uncurry each) i'

printIndexDocument ::
  (FilePath -> FilePath)
  -> DocumentMetadata
  -> Index
  -> Doc String
printIndexDocument lk d i =
  let title = "Air Law - Australia"
  in  line (pure "<!doctype html>") <>
      tag "html" [("lang", "en")] (
        printIndexHead title <>
        tag "body" [] (
          tag "main" [] (
            tag "div" [("class", "container-fluid")] (
              printTitleContainer d title <>
              printIndexContainer lk i
            )
          )
        )
      )

printIndexHead ::
  String
  -> Doc String
printIndexHead t =
  tag "head" [] (
    pure (openingTag "meta" [("charset", "utf-8")]) <>
    pure (openingTag "meta" [("http-equiv", "x-ua-compatible"), ("content", "ie=edge")]) <>
    pure (openingTag "meta" [("name", "viewport"), ("content", "width=device-width, initial-scale=1, shrink-to-fit=no")]) <>
    inlineTag "title" [] (pure t) <>
    pure (openingTag "link" [("rel", "stylesheet"), ("href", "css/OpenSans.css"), ("type", "text/css")]) <>
    pure (openingTag "link" [("rel", "stylesheet"), ("href", "css/bootstrap.css")]) <>
    pure (openingTag "link" [("rel", "stylesheet"), ("href", "css/font-awesome.css")]) <>
    pure (openingTag "link" [("rel", "stylesheet"), ("href", "css/custom.css")])
  )

data DocumentMetadata =
  DocumentMetadata
    String -- hosted link
    String -- source hosted at
    String -- last updated time
    String -- last updated user name
    String -- last updated user email
    String -- revision
    String -- access control
  deriving (Eq, Ord, Show)

printDocumentMetadata ::
  DocumentMetadata
  -> Doc String
printDocumentMetadata (DocumentMetadata h s t n e r a) =
  let code v =
        tag "code" [("style", "display: inline")] (pure v)
      ahrefCode v w =
        tag "a" [("href", v)] (code w)
      sect =
        tag "div" [("class", "text-left"), ("style", "font-size: 10px")]
      metaLink' x u u' =
        sect (
          pure x <>
          ahrefCode u u'
        )
      metaLink x u =
        metaLink' x u u
      metaNoLink x u =
        sect (pure x <> code u)
      metaUser x n' u =
        sect (
          pure x <>
          code n' <>
          ahrefCode ("mailto:" <> u) u
        )
  in  tag "div" [] (
        metaLink "hosted at" h <>
        metaLink "source hosted at" s <>
        metaNoLink "last updated at time" t <>
        metaUser "last updated by user" n (escape ("<" <> e <> ">")) <>
        metaLink' "revision" (s <> "/-/commit/" <> r) r <>
        metaNoLink "access control" a <>
        tag "div" [("class", "text-left")] (
          tag "div" [("class", "text-left")] (
            tag "a" [("href", s <> "/-/pipelines/latest")] (
              pure (openingTag "img" [("src", s <> "/badges/master/pipeline.svg"), ("alt", "Pipeline status")])
            )
          )
        )
      )

printTitleContainer ::
  DocumentMetadata
  -> String
  -> Doc String
printTitleContainer d t =
  printDocumentMetadata d <>
  pure (openingTag "hr" []) <>
  tag "div" [("class", "container my-4")] (
    tag "div" [("class", "row")] (
      tag "div" [("class", "col")] (
        tag "h1" [("class", "display-4 text-center")] (
          pure t
        )
      )
    )
  )

printIndexContainer ::
  (FilePath -> FilePath)
  -> Index
  -> Doc String
printIndexContainer lk i =
  tag "div" [("class", "container")] (tag "div" [("class", "row")] (tag "div" [("class", "col")] (printIndex lk i)))

---- Printer library

anchorId ::
  String
  -> String
anchorId s =
  s >>= \case
    ' ' ->
      "_"
    '_' ->
      "__"
    c ->
      [c]

escape ::
  String
  -> String
escape s =
  s >>= \case
          '&' ->
            "&amp;"
          '<' ->
            "&lt;"
          '>' ->
            "&gt;"
          '"' ->
            "&quot;"
          '\'' ->
            "&#39;"
          c ->
            [c]

data Layout a =
  Space
  | Content a
  deriving (Eq, Ord, Show)

layout ::
  b
  -> (a -> b)
  -> Layout a
  -> b
layout sp f c =
  case c of
    Space ->
      sp
    Content a ->
      f a

instance Functor Layout where
  fmap _ Space =
    Space
  fmap f (Content a) =
    Content (f a)

instance Applicative Layout where
  pure =
    Content
  Space <*> _ =
    Space
  _ <*> Space =
    Space
  Content f <*> Content a =
    Content (f a)

instance Monad Layout where
  return =
    Content
  Space >>= _ =
    Space
  Content a >>= f =
    f a

instance Foldable Layout where
  foldMap _ Space =
    mempty
  foldMap f (Content a) =
    f a

instance Traversable Layout where
  traverse _ Space =
    pure Space
  traverse f (Content a) =
    Content <$> f a

newtype Line a =
  Line {
    unLine ::
      [Layout a]
  }
  deriving (Eq, Ord, Show)

instance Functor Line where
  fmap f (Line x) =
    Line (fmap (fmap f) x)

instance Applicative Line where
  pure =
    Line . pure . pure
  Line f <*> Line a =
    Line (liftA2 (<*>) f a)

instance Monad Line where
  return =
    Line . return . return
  Line x >>= f =
    Line (x >>= \case
                  Space ->
                    pure Space
                  Content a ->
                    unLine (f a))

instance Foldable Line where
  foldMap f (Line x) =
    foldMap (foldMap f) x

instance Traversable Line where
  traverse f (Line x) =
    Line <$> traverse (traverse f) x

-- | The 'Semigroup' instance for 'Line' performs horizontal concatenation
instance Semigroup (Line a) where
  Line a <> Line b =
    Line (a <> b)

instance Monoid (Line a) where
  -- | The empty line
  mempty =
    Line mempty

newtype Doc a =
  Doc {
    unDoc ::
      [Line a]
  }
  deriving (Eq, Ord, Show)

instance Functor Doc where
  fmap f (Doc x) =
    Doc (fmap (fmap f) x)

instance Applicative Doc where
  pure =
    Doc . pure . pure
  Doc f <*> Doc a =
    Doc (liftA2 (<*>) f a)

instance Monad Doc where
  return =
    Doc . return . return
  Doc x >>= f =
    Doc (x >>= \(Line x') ->
      x' >>= \case Space -> pure (Line (pure Space))
                   Content a -> unDoc (f a))

instance Foldable Doc where
  foldMap f (Doc x) =
    foldMap (foldMap f) x

instance Traversable Doc where
  traverse f (Doc x) =
    Doc <$> traverse (traverse f) x

-- | The 'Semigroup' instance for 'Doc' performs vertical concatenation
instance Semigroup (Doc a) where
  Doc a <> Doc b =
    Doc (a <> b)

-- | The empty document
instance Monoid (Doc a) where
  mempty =
    Doc mempty

line ::
  Line a
  -> Doc a
line l =
  Doc (pure l)

-- | A document with all lines indented by @n@ spaces
--
-- @indented m (indented n d) = indented (m + n) d@
indented ::
  Int
  -> Doc a
  -> Doc a
indented n (Doc ls) =
  Doc (Line . (replicate n Space <>) . unLine <$> ls)

render ::
  Monoid m =>
  -- How to display a space
  m
  -- How to display a newline
  -> m
  -- How to display content
  -> (a -> m)
  -> Doc a
  -> m
render sp nl f = go . unDoc
  where
    go ls =
      case ls of
        [] -> mempty
        Line l : ls' ->
          foldMap (layout sp f) l <>
          case ls' of
            [] -> mempty
            _:_ -> nl <> go ls'

tag ::
  String
  -> [(String, String)]
  -> Doc String
  -> Doc String
tag =
  tag' pure pure

tag' ::
  (String -> Doc a) -- on opening tag
  -> (String -> Doc a) -- on closing tag
  -> String
  -> [(String, String)]
  -> Doc a
  -> Doc a
tag' f g t attr p =
  f (openingTag t attr) <>
  indented 1 p <>
  g ("</" <> t <> ">")

inlineTag ::
  String
  -> [(String, String)]
  -> Line String
  -> Doc String
inlineTag =
  inlineTag' pure pure

inlineTag' ::
  (String -> Line a)
  -> (String -> Line a)
  -> String
  -> [(String, String)]
  -> Line a
  -> Doc a
inlineTag' f g t attr p =
  line (f (openingTag t attr) <>
        p <>
        g ("</" <> t <> ">"))

openingTag ::
  String
  -> [(String, String)]
  -> String
openingTag t attr =
  "<" <> t <> (attr >>= \(k, v) -> ' ' : escape k <> "=\"" <> escape v <> "\"") <> ">"

---- index parser

parseIndexEntry ::
  Parser (NonEmpty Char, IndexValue)
parseIndexEntry =
  let notComma =
        list1 (satisfy "not equal to comma or newline" (`notElem` ",\n"))
      comma =
        satisfy "equal to comma" (== ',')
      newline =
        satisfy "equal to newline" (== '\n')
      parseIndexValue =
        IndexValue <$> notComma <* comma <*> notComma
  in  (,) <$> notComma <* comma <*> parseIndexValue <* newline

parseIndexEntries ::
  Parser [(NonEmpty Char, IndexValue)]
parseIndexEntries =
  list parseIndexEntry

parseIndex ::
  Parser Index
parseIndex =
  makeIndex <$> parseIndexEntries

---- Parser library

data ParseResult a =
  ParseError String
  | ParseSuccess String a
  deriving (Eq, Ord, Show)

instance Functor ParseResult where
  fmap _ (ParseError e) =
    ParseError e
  fmap f (ParseSuccess s a) =
    ParseSuccess s (f a)

liftParseResult ::
  ParseResult a
  -> Parser a
liftParseResult =
  Parser . const

newtype Parser a =
  Parser {
    parse ::
      String
      -> ParseResult a
  }

instance Functor Parser where
  fmap f (Parser p) =
    Parser (fmap f . p)

instance Applicative Parser where
  pure =
    return
  (<*>) =
    ap

instance Monad Parser where
  return a =
    Parser (`ParseSuccess` a)
  Parser p >>= f =
    Parser (\s -> case p s of
                    ParseError e -> ParseError e
                    ParseSuccess s' a -> parse (f a) s')

(|||) ::
  Parser a
  -> Parser a
  -> Parser a
Parser p1 ||| Parser p2 =
  Parser (\s -> case p1 s of
                  ParseError _ ->
                    p2 s
                  ParseSuccess r a ->
                    ParseSuccess r a)

char ::
  Parser Char
char =
  Parser (\case
    [] -> ParseError "Unexpected EOF"
    h:t -> ParseSuccess t h)

satisfy ::
  String
  -> (Char -> Bool)
  -> Parser Char
satisfy e p =
  char >>= \c -> if p c then pure c else liftParseResult (ParseError e)

list ::
  Parser a
  -> Parser [a]
list k =
  fmap toList (list1 k) ||| pure []

list1 ::
  Parser a
  -> Parser (NonEmpty a)
list1 k =
  k >>= \k' ->
  list k >>= \ks ->
  pure (k' :| ks)


----


readMaybe ::
  Read a =>
  String
  -> Maybe a
readMaybe x =
  case reads x of
    [] ->
      Nothing
    ((h, _):_) ->
      Just h

-- |
--
-- >>> sortDigits "abc" "def"
-- LT
-- >>> sortDigits "100" "200"
-- LT
-- >>> sortDigits "100" "95"
-- GT
sortDigits ::
  String
  -> String
  -> Ordering
sortDigits x y =
  let readInt ::
        String
        -> Maybe Int
      readInt =
        readMaybe
      i =
        readInt x >>= \x' ->
        readInt y >>= \y' ->
        pure (x' `compare` y')
  in  fromMaybe (x `compare` y) i

-- |
--
-- >>> sortDigitList "abc" "def"
-- LT
-- >>> sortDigitList "100" "200"
-- LT
-- >>> sortDigitList "100" "95"
-- GT
-- >>> sortDigitList "65" "100"
-- LT
-- >>> sortDigitList "100.1" "100"
-- GT
-- >>> sortDigitList "100" "100.1"
-- LT
-- >>> sortDigitList "100.100" "100.95"
-- GT
-- >>> sortDigitList "100.100" "100.100.2"
-- LT
sortDigitList ::
  (Foldable f1, Foldable f2) =>
  f1 Char
  -> f2 Char
  -> Ordering
sortDigitList x y =
  let r = align (groupBy (\a1 a2 -> isDigit a1 && isDigit a2) x) (groupBy (on (&&) isDigit) y)
      t1 = view these r
      t2 = view those r
  in  foldMap (uncurry (on sortDigits toList)) t1 <> maybe EQ (either (const GT) (const LT)) t2

-- |
--
-- >>> sortPriorityWords ["xyz"] "abc" "def"
-- LT
-- >>> sortPriorityWords [] "xyz" "zyx"
-- LT
-- >>> sortPriorityWords ["xyz"] "xyz" "zyx"
-- GT
-- >>> sortPriorityWords ["abc", "xyz"] "xyz" "zyx"
-- GT
-- >>> sortPriorityWords ["abc", "xyz"] "xyz" "abc"
-- LT
sortPriorityWords ::
  (Ord (f Char), Functor f, Foldable f) =>
  [f Char]
  -> f Char
  -> f Char
  -> Ordering
sortPriorityWords w x y =
  let lower =
        fmap toLower
      compareWords a =
        let a' =
              lower a
        in  if a' == lower x
              then
                Just (Left x)
              else
                if a' == lower y
                  then
                    Just (Right y)
                  else
                    Nothing
  in  case mapMaybe compareWords w of
        [] ->
          x `sortDigitList` y
        (Left _:_) ->
          LT
        (Right _:_) ->
          GT
